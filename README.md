# PHP Select2

[
	![](https://badgen.net/packagist/v/samy/select2/latest)
	![](https://badgen.net/packagist/license/samy/select2)
	![](https://badgen.net/packagist/dt/samy/select2)
	![](https://badgen.net/packagist/favers/samy/select2)
](https://packagist.org/packages/samy/select2)

This is a simple way to implement Select2 server-side processing.

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require samy/select2
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/php-select2>
* Documentations: <https://monstm.gitlab.io/php-select2/>
* Annotation: <https://monstm.alwaysdata.net/php-select2/>
* Issues: <https://gitlab.com/monstm/php-select2/-/issues>
