# Select2 Interface

Describes Select2 interface.

---

## withSchema

Return an instance with provided schema.

```php
$select2 = $select2->withSchema($table_name, $field_id, $field_text = "", $field_selected, $field_disabled = "", $field_group = "");
```

---

## withFilter

Return an instance with provided filter.

```php
$filter = array(
	"field_string" => "lorem ipsum",
	"field_number" => 123,
	"field_time" => "NOW()",
	"`field_sql` = 'complex query'"
);

$select2 = $select2->withFilter($filter);
```

---

## withLimit

Return an instance with provided query limit.

```php
$select2 = $select2->withLimit($limit);
```

---

## getResponse

Retrieve Select2 response.

```php
$response = $select2->getResponse($_GET);
```

---

## escape

Escapes special characters in a string for use in an SQL statement.

```php
$escaped_string = $select2->escape($unescaped_string);
```
