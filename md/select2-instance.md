# Select2 Instance

Simple Select2 Implementation.

---

## MySQL

Implementation MySQL instance.

```php
$select2 = MySql($host, $username, $password, $database, $port = 3306);
```

---

## PostgreSQL

Implementation PostgreSQL instance.

```php
$select2 = new PostgreSql($host, $username, $password, $database, $port = 5432);
```
