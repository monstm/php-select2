<?php

namespace Test\Unit;

use PHPUnit\Framework\TestCase;

class AbstractSelect2 extends TestCase
{
    protected $select2;

    protected $table_name = "php_select2_country";
    protected $field_id = "code";
    protected $field_text = "name";


    /**
     * @depends testSqlInit
     * @dataProvider \Test\Unit\DataProvider::dataResponse
     */
    public function testResponse($Filter, $Limit, $Request, $Results, $MorePagination): void
    {
        $response = $this->select2
            ->withSchema($this->table_name, $this->field_id, $this->field_text)
            ->withFilter($Filter)
            ->withLimit($Limit)
            ->getResponse($Request);


        $this->assertArrayHasKey("results", $response);

        $response_results = ($response["results"] ?? array());
        $this->assertSame($Results, $response_results);


        $this->assertArrayHasKey("pagination", $response);

        $response_pagination = ($response["pagination"] ?? array());
        $this->assertArrayHasKey("more", $response_pagination);

        $response_pagination_more = ($response_pagination["more"] ?? null);
        $this->assertSame($MorePagination, $response_pagination_more);
    }
}
