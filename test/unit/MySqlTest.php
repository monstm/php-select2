<?php

namespace Test\Unit;

use Samy\Select2\MySql;
use Samy\Sql\MySql as SqlMySql;

class MySqlTest extends AbstractSelect2
{
    protected function setUp(): void
    {
        $this->select2 = new MySql(
            MYSQL_HOST,
            MYSQL_USERNAME,
            MYSQL_PASSWORD,
            MYSQL_DATABASE,
            intval(MYSQL_PORT)
        );
    }


    public function testSqlInit(): void
    {
        $mysql = new SqlMySql(
            MYSQL_HOST,
            MYSQL_USERNAME,
            MYSQL_PASSWORD,
            MYSQL_DATABASE,
            intval(MYSQL_PORT)
        );

        $is_connected = $mysql->isConnected();

        $this->assertTrue($is_connected);

        if ($is_connected) {
            $query = array(
                "DROP TABLE IF EXISTS `" . $this->table_name . "`",
                "CREATE TABLE `" . $this->table_name . "`(" .
                    "`id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY, " .
                    "`code` varchar(2) NOT NULL, " .
                    "`name` varchar(64) NOT NULL" .
                    ")"
            );


            $scv = __DIR__ . DIRECTORY_SEPARATOR . "country.csv";

            if (is_file($scv)) {
                $file = fopen($scv, "r");
                if ($file) {
                    while (($data = fgetcsv($file)) !== false) {
                        $count = count($data);

                        array_push(
                            $query,
                            "INSERT INTO `" . $this->table_name . "`(`code`, `name`) " .
                                "VALUES(" .
                                "'" . ($count > 0 ? $mysql->escape($data[0]) : "") . "', " .
                                "'" . ($count > 1 ? $mysql->escape($data[1]) : "") . "'" .
                                ")"
                        );
                    }

                    fclose($file);
                }
            }


            foreach ($query as $command) {
                $mysql->execute($command);
            }
        }
    }

    /**
     * @depends testSqlInit
     * @dataProvider \Test\Unit\MySqlDataProvider::dataSqlEscapeString
     */
    public function testEscapeString($UnescapedString, $EscapedString): void
    {
        $this->assertSame($EscapedString, $this->select2->escape($UnescapedString));
    }
}
