<?php

namespace Test\Unit;

class DataProvider
{
    // $Filter, $Limit, $Request, $Results, $MorePagination
    public function dataResponse(): array
    {
        $ret = array();

        $filename = __DIR__ . DIRECTORY_SEPARATOR . "response.json";

        if (is_file($filename)) {
            $json = json_decode(file_get_contents($filename), true);
            if (is_array($json)) {
                foreach ($json as $data) {
                    array_push($ret, array(
                        ($data["filter"] ?? array()), // $Filter
                        ($data["limit"] ?? 0), // $Limit
                        ($data["request"] ?? array()), // $Request
                        ($data["result"] ?? array()), // $Results
                        ($data["more_pagination"] ?? false) // $MorePagination
                    ));
                }
            }
        }

        return $ret;
    }
}
