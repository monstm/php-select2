<?php

namespace Test\Unit;

use Samy\Select2\PostgreSql;
use Samy\Sql\PostgreSql as SqlPostgreSql;

class PostgreSqlTest extends AbstractSelect2
{
    protected function setUp(): void
    {
        $this->select2 = new PostgreSql(
            POSTGRESQL_HOST,
            POSTGRESQL_USERNAME,
            POSTGRESQL_PASSWORD,
            POSTGRESQL_DATABASE,
            intval(POSTGRESQL_PORT)
        );
    }


    public function testSqlInit(): void
    {
        $mysql = new SqlPostgreSql(
            POSTGRESQL_HOST,
            POSTGRESQL_USERNAME,
            POSTGRESQL_PASSWORD,
            POSTGRESQL_DATABASE,
            intval(POSTGRESQL_PORT)
        );

        $is_connected = $mysql->isConnected();

        $this->assertTrue($is_connected);

        if ($is_connected) {
            $query = array(
                "DROP TABLE IF EXISTS " . $this->table_name . " CASCADE",
                "CREATE TABLE " . $this->table_name . "(" .
                    "\"id\" serial NOT NULL, " .
                    "PRIMARY KEY (\"id\"), " .
                    "\"code\" character varying(2) NOT NULL, " .
                    "\"name\" character varying(64) NOT NULL" .
                    ")"
            );


            $scv = __DIR__ . DIRECTORY_SEPARATOR . "country.csv";

            if (is_file($scv)) {
                $file = fopen($scv, "r");
                if ($file) {
                    while (($data = fgetcsv($file)) !== false) {
                        $count = count($data);

                        array_push(
                            $query,
                            "INSERT INTO \"" . $this->table_name . "\"(\"code\", \"name\") " .
                                "VALUES(" .
                                "'" . ($count > 0 ? $mysql->escape($data[0]) : "") . "', " .
                                "'" . ($count > 1 ? $mysql->escape($data[1]) : "") . "'" .
                                ")"
                        );
                    }

                    fclose($file);
                }
            }


            foreach ($query as $command) {
                $mysql->execute($command);
            }
        }
    }

    /**
     * @depends testSqlInit
     * @dataProvider \Test\Unit\PostgreSqlDataProvider::dataSqlEscapeString
     */
    public function testEscapeString($UnescapedString, $EscapedString): void
    {
        $this->assertSame($EscapedString, $this->select2->escape($UnescapedString));
    }
}
