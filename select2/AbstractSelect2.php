<?php

namespace Samy\Select2;

/**
 * This is a simple Select2 implementation that other Select2 can inherit from.
 */
abstract class AbstractSelect2 implements Select2Interface
{
    /** describe driver */
    protected $driver = null;

    /** describe table name */
    protected $table = "";

    /** describe field id */
    protected $field_id = "";

    /** describe field text */
    protected $field_text = "";

    /** describe field selected */
    protected $field_selected = "";

    /** describe field disabled */
    protected $field_disabled = "";

    /** describe field group */
    protected $field_group = "";

    /** describe has schema */
    protected $has_schema = false;

    /** describe filter data */
    protected $filter_data = array();

    /** describe result limit */
    protected $result_limit = 10;


    /**
     * Retrieve SQL filter expression.
     *
     * @param[in] string $TableName Table name
     * @param[in] string $FieldName Field name
     * @param[in] mixed $Value Data value
     *
     * @return string
     */
    abstract protected function sqlFilter(string $TableName, string $FieldName, mixed $Value): string;

    /**
     * Retrieve SQL search expression.
     *
     * @param[in] string $TableName Table name
     * @param[in] string $FieldName Field name
     * @param[in] mixed $Value Data value
     *
     * @return string
     */
    abstract protected function sqlSearch(string $TableName, string $FieldName, mixed $Value): string;

    /**
     * Retrieve SQL order expression.
     *
     * @param[in] string $TableName Table name
     * @param[in] string $FieldName Field name
     * @param[in] bool $Ascending Ascending order
     *
     * @return string
     */
    abstract protected function sqlOrder(string $TableName, string $FieldName, bool $Ascending): string;


    /**
     * Retrieve record count.
     *
     * @param[in] array $Condition SQL condition
     *
     * @return int
     */
    abstract protected function recordCount(array $Condition): int;

    /**
     * Retrieve record data.
     *
     * @param[in] array $Condition SQL condition
     * @param[in] array $Order SQL order
     * @param[in] int $Offset SQL offset
     * @param[in] int $Limit SQL limit
     *
     * @return array<array<string, mixed>>
     */
    abstract protected function recordData(array $Condition, array $Order, int $Offset, int $Limit): array;


    /**
     * Interpolates context values into the message placeholders.
     *
     * @param[in] string $message
     * @param[in] array $context
     *
     * @return string
     */
    protected function interpolate(string $message, array $context = array()): string
    {
        $replace_pairs = array();
        foreach ($context as $key => $value) {
            if (is_string($key) && is_string($value)) {
                $replace_pairs["{" . $key . "}"] = $value;
            }
        }

        return strtr($message, $replace_pairs);
    }


    /**
     * Return an instance with provided schema.
     *
     * @param[in] string $TableName Table name
     * @param[in] string $FieldId Field id
     * @param[in] string $FieldText Field text
     * @param[in] string $FieldSelected Field selected
     * @param[in] string $FieldDisabled Field disabled
     * @param[in] string $FieldGroup Field group
     *
     * @return static
     */
    public function withSchema(
        string $TableName,
        string $FieldId,
        string $FieldText = "",
        string $FieldSelected = "",
        string $FieldDisabled = "",
        string $FieldGroup = ""
    ): self {
        $this->table = $TableName;
        $this->field_id = $FieldId;
        $this->field_text = ($FieldText != "" ? $FieldText : $FieldId);
        $this->field_selected = $FieldSelected;
        $this->field_disabled = $FieldDisabled;
        $this->field_group = $FieldGroup;

        $this->has_schema = (($this->table != "") && ($this->field_id != ""));

        return $this;
    }

    /**
     * Return an instance with provided filter.
     *
     * @param[in] array $Data Filter data
     *
     * @return static
     */
    public function withFilter(array $Data): self
    {
        $this->filter_data = $Data;

        return $this;
    }

    /**
     * Return an instance with provided query limit.
     *
     * @param[in] int $Limit Results limit
     *
     * @return static
     */
    public function withLimit(int $Limit): self
    {
        if ($Limit > 0) {
            $this->result_limit = $Limit;
        }

        return $this;
    }


    /**
     * Retrieve Select2 response.
     *
     * @param[in] array $Request Select2 request
     *
     * @return array<array<string, mixed>>
     */
    public function getResponse(array $Request): array
    {
        $ret = array(
            "results" => array(),
            "pagination" => array("more" => false)
        );

        if ($this->has_schema) {
            $q = ($Request["q"] ?? "");
            $terms = ($Request["term"] ?? $q);

            $page = intval($Request["page"] ?? "1");
            if ($page < 1) {
                $page = 1;
            }


            $filter_data = $this->filterData();
            $filter_search = $this->filterSearch($terms);
            $filter_merge = array_merge($filter_data, $filter_search);

            $order_data = $this->orderData();

            $offset = ($page - 1) * $this->result_limit;


            $record_data = $this->recordData($filter_merge, $order_data, $offset, $this->result_limit);
            $ret["results"] = $this->resultFormat($record_data);

            $record_count = $this->recordCount($filter_merge);
            $ret["pagination"]["more"] = (($offset + count($record_data)) < $record_count);
        }

        return $ret;
    }


    /**
     * Escapes special characters in a string for use in an SQL statement.
     *
     * Escapes special characters in the unescaped_string,
     * taking into account the current character set of the connection
     * so that it is safe to place it in a execute() or query().
     * If binary data is to be inserted, this function must be used.
     *
     * This function must always (with few exceptions) be used to make data safe before sending a query.
     *
     * @param[in] string $UnescapedString The string that is to be escaped
     *
     * @return string Escaped query
     */
    public function escape(string $UnescapedString): string
    {
        return ($this->driver->isConnected() ? $this->driver->escape($UnescapedString) : $UnescapedString);
    }


    /**
     * Retrieve SQL data filter.
     *
     * @return array<string>
     */
    private function filterData(): array
    {
        $ret = array();

        foreach ($this->filter_data as $field => $data) {
            $result = "";

            switch (gettype($field)) {
                case "integer":
                    if (is_string($data)) {
                        $result = trim($data);
                    }
                    break;
                case "string":
                    $result = trim($this->sqlFilter($this->table, $field, $data));
                    break;
            }

            if ($result != "") {
                array_push($ret, "(" . $result . ")");
            }
        }

        return $ret;
    }

    /**
     * Retrieve SQL search filter.
     *
     * @param[in] string $Terms The current search term in the search box.
     *
     * @return array<string>
     */
    private function filterSearch(string $Terms): array
    {
        $ret = array();

        foreach (explode(" ", $Terms) as $term) {
            $trim = trim($term);

            if ($trim != "") {
                $result = trim($this->sqlSearch($this->table, $this->field_text, $trim));

                if ($result != "") {
                    array_push($ret, "(" . $result . ")");
                }
            }
        }

        return $ret;
    }


    /**
     * Retrieve SQL data order.
     *
     * @return array<string>
     */
    private function orderData(): array
    {
        $ret = array();
        $unique = array();
        $orders = array(
            array("field" => $this->field_group, "ascending" => true),
            array("field" => $this->field_text, "ascending" => true),
            array("field" => $this->field_id, "ascending" => true),
            array("field" => $this->field_selected, "ascending" => true),
            array("field" => $this->field_disabled, "ascending" => false)
        );

        foreach ($orders as $order) {
            if (($order["field"] != "") && !in_array($order["field"], $unique)) {
                $result = trim($this->sqlOrder($this->table, $order["field"], $order["ascending"]));

                if ($result != "") {
                    array_push($ret, $result);
                }


                array_push($unique, $order["field"]);
            }
        }

        return $ret;
    }


    /**
     * Retrieve Select2 result format.
     *
     * @param[in] array $Data Record data
     *
     * @return array<array<string, mixed>>
     */
    private function resultFormat(array $Data): array
    {
        $ret = array();
        $groups = array();

        foreach ($Data as $record) {
            $group_name = trim($record["group"]);
            if ($group_name != "") {
                if (!isset($groups[$group_name])) {
                    $groups[$group_name] = array();
                }

                array_push($groups[$group_name], array(
                    "id" => $record["id"],
                    "text" => $record["text"],
                    "selected" => filter_var(strtolower($record["selected"]), FILTER_VALIDATE_BOOLEAN),
                    "disabled" => filter_var(strtolower($record["disabled"]), FILTER_VALIDATE_BOOLEAN)
                ));
            } else {
                array_push($ret, array(
                    "id" => $record["id"],
                    "text" => $record["text"],
                    "selected" => filter_var(strtolower($record["selected"]), FILTER_VALIDATE_BOOLEAN),
                    "disabled" => filter_var(strtolower($record["disabled"]), FILTER_VALIDATE_BOOLEAN)
                ));
            }
        }


        foreach ($groups as $group_name => $groups_data) {
            array_push($ret, array(
                "text" => $group_name,
                "children" => $groups_data
            ));
        }


        return $ret;
    }
}
