<?php

namespace Samy\Select2;

use Samy\Sql\PostgreSql as SqlPostgreSql;

/**
 * Simple PostgreSQL implementation.
 */
class PostgreSql extends AbstractSelect2
{
    /**
     * PostgreSql construction.
     *
     * @param[in] string $Host PostgreSQL Host
     * @param[in] string $Username PostgreSQL Password
     * @param[in] string $Password PostgreSQL Username
     * @param[in] string $Database PostgreSQL Database
     * @param[in] int $Port PostgreSQL Port
     *
     * @return void
     */
    public function __construct(string $Host, string $Username, string $Password, string $Database, int $Port = 3306)
    {
        $this->driver = new SqlPostgreSql($Host, $Username, $Password, $Database, $Port);
    }


    /**
     * Retrieve SQL filter expression.
     *
     * @param[in] string $TableName Table name
     * @param[in] string $FieldName Field name
     * @param[in] mixed $Value Data value
     *
     * @return string
     */
    protected function sqlFilter(string $TableName, string $FieldName, mixed $Value): string
    {
        $quote = false;

        if (is_string($Value)) {
            if ($this->driver->isFunction($Value)) {
                $data = $Value;
            } else {
                $data = $this->driver->escape($Value);
                $quote = true;
            }
        } elseif (is_null($Value)) {
            $data = "null";
        } else {
            $data = strval($Value);
        }


        return $this->interpolate("\"{table}\".\"{field}\" = {quote}{data}{quote}", array(
            "table" => $TableName,
            "field" => $FieldName,
            "quote" => ($quote ? "'" : ""),
            "data" => $data
        ));
    }

    /**
     * Retrieve SQL search expression.
     *
     * @param[in] string $TableName Table name
     * @param[in] string $FieldName Field name
     * @param[in] mixed $Value Data value
     *
     * @return string
     */
    protected function sqlSearch(string $TableName, string $FieldName, mixed $Value): string
    {
        $operator = "=";
        $quote = false;

        if (is_string($Value)) {
            if ($this->driver->isFunction($Value)) {
                $data = $Value;
            } else {
                $data = "%" . $this->driver->escape($Value) . "%";
                $operator = "ILIKE";
                $quote = true;
            }
        } elseif (is_null($Value)) {
            $data = "null";
        } else {
            $data = strval($Value);
        }


        return $this->interpolate("\"{table}\".\"{field}\" {operator} {quote}{data}{quote}", array(
            "table" => $TableName,
            "field" => $FieldName,
            "operator" => $operator,
            "quote" => ($quote ? "'" : ""),
            "data" => $data
        ));
    }

    /**
     * Retrieve SQL order expression.
     *
     * @param[in] string $TableName Table name
     * @param[in] string $FieldName Field name
     * @param[in] bool $Ascending Ascending order
     *
     * @return string
     */
    protected function sqlOrder(string $TableName, string $FieldName, bool $Ascending): string
    {
        return $this->interpolate("\"{table}\".\"{field}\" {order}", array(
            "table" => $TableName,
            "field" => $FieldName,
            "order" => ($Ascending ? "ASC" : "DESC")
        ));
    }


    /**
     * Retrieve record count.
     *
     * @param[in] array $Condition SQL condition
     *
     * @return int
     */
    protected function recordCount(array $Condition): int
    {
        $query = "SELECT COUNT(0) AS \"count\" " .
            "FROM \"" . $this->table . "\"" .
            (count($Condition) > 0 ? " WHERE " . implode(" AND ", $Condition) : "");

        $result = $this->driver->query($query);


        return (count($result) > 0 ? $result[0]["count"] : 0);
    }

    /**
     * Retrieve record data.
     *
     * @param[in] array $Condition SQL condition
     * @param[in] array $Order SQL order
     * @param[in] int $Offset SQL offset
     * @param[in] int $Limit SQL limit
     *
     * @return array<array<string, mixed>>
     */
    protected function recordData(array $Condition, array $Order, int $Offset, int $Limit): array
    {
        $field_id = (!empty($this->field_id) ? "\"" . $this->table . "\".\"" . $this->field_id . "\"" : "''");
        $field_text = (!empty($this->field_text) ? "\"" . $this->table . "\".\"" . $this->field_text . "\"" : "''");
        $field_selected = (!empty($this->field_selected) ?
            "\"" . $this->table . "\".\"" . $this->field_selected . "\"" :
            "0"
        );
        $field_disabled = (!empty($this->field_disabled) ?
            "\"" . $this->table . "\".\"" . $this->field_disabled . "\"" :
            "0"
        );
        $field_group = (!empty($this->field_group) ? "\"" . $this->table . "\".\"" . $this->field_group . "\"" : "''");

        $query = "SELECT " .
            $field_id . " AS \"id\", " .
            $field_text . " AS \"text\", " .
            $field_selected . " AS \"selected\", " .
            $field_disabled . " AS \"disabled\", " .
            $field_group . " AS \"group\" " .
            "FROM \"" . $this->table . "\"" .
            (count($Condition) > 0 ? " WHERE " . implode(" AND ", $Condition) : "") .
            (count($Order) > 0 ? " ORDER BY " . implode(", ", $Order) : "") .
            ($Limit > 0 ? " LIMIT " . $Limit : "") .
            ($Offset > 0 ? " OFFSET " . $Offset : "");


        return $this->driver->query($query);
    }
}
