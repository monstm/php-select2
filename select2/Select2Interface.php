<?php

namespace Samy\Select2;

/**
 * Describes Select2 interface.
 */
interface Select2Interface
{
    /**
     * Return an instance with provided schema.
     *
     * @param[in] string $TableName Table name
     * @param[in] string $FieldId Field id
     * @param[in] string $FieldText Field text
     * @param[in] string $FieldSelected Field selected
     * @param[in] string $FieldDisabled Field disabled
     * @param[in] string $FieldGroup Field group
     *
     * @return static
     */
    public function withSchema(
        string $TableName,
        string $FieldId,
        string $FieldText = "",
        string $FieldSelected = "",
        string $FieldDisabled = "",
        string $FieldGroup = ""
    ): self;

    /**
     * Return an instance with provided filter.
     *
     * @param[in] array $Data Filter data
     *
     * @return static
     */
    public function withFilter(array $Data): self;

    /**
     * Return an instance with provided query limit.
     *
     * @param[in] int $Limit Query limit
     *
     * @return static
     */
    public function withLimit(int $Limit): self;

    /**
     * Retrieve Select2 response.
     *
     * @param[in] array $Request Select2 request
     *
     * @return array<array<string, mixed>>
     */
    public function getResponse(array $Request): array;


    /**
     * Escapes special characters in a string for use in an SQL statement.
     *
     * Escapes special characters in the unescaped_string,
     * taking into account the current character set of the connection
     * so that it is safe to place it in a execute() or query().
     * If binary data is to be inserted, this function must be used.
     *
     * This function must always (with few exceptions) be used to make data safe before sending a query.
     *
     * @param[in] string $UnescapedString The string that is to be escaped
     *
     * @return string Escaped query
     */
    public function escape(string $UnescapedString): string;
}
